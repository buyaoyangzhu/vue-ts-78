import { createStore } from "vuex";

const store = createStore({
  // 数据
  state: {
    money: 10000,
  },
  //   计算属性
  getters: {
    double(state) {
      return state.money * 2;
    },
  },
  //   只能写同步的
  mutations: {
    addMoney(state, val) {
      state.money += val;
    },
  },
  //   异步需要写在actions
  actions: {
    addMoneyAsync(context, val) {
      setTimeout(() => {
        context.commit("addMoney", val);
      }, 2000);
    },
  },
});

export default store;

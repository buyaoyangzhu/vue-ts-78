import { createPinia } from "pinia";
import { createApp } from "vue";
import App from "./App.vue";
import "./styles/base.css";
import "./styles/index.css";

const pinia = createPinia();

// 任何插件，都是在创建完app实例之后，渲染到页面之前使用
createApp(App).use(pinia).mount("#app");

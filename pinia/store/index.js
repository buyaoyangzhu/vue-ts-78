// 用来做总主管，把所有用到的状态都引过来，再导出去
import useCounterStore from "./modules/counter";
import useUserStore from "./modules/user";

// 调用后拿到{counter，user}状态
const useStore = () => {
  return {
    counter: useCounterStore(),
    user: useUserStore(),
  };
};

export default useStore;

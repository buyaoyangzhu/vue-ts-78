import { defineStore } from "pinia";

// 创建store,命名规则： useXxxxStore
const useCounterStore = defineStore("counter", {
  // vue2 里面的data
  state() {
    return {
      count: 10,
    };
  },
  //   等同于vue2里面的计算属性
  getters: {
    double() {
      return this.count * 2;
    },
  },
  //   等同于vue2里面的方法methods
  actions: {
    add() {
      this.count++;
    },
    addNum(val) {
      this.count += val;
    },
    addAsync(val) {
      setTimeout(() => {
        this.count += val;
      }, 1000);
    },
  },
});

export default useCounterStore;

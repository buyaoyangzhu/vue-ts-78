import { defineStore } from "pinia";
// vue2 模块化
// this.$store.user.state
// this.$store.counter.state

const useUserStore = defineStore("user", {
  state: () => {
    return {
      name: "zs",
      age: 100,
    };
  },
});

export default useUserStore;
